import requests
import os
import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_riwayat_list_URL = "https://private-e52a5-ppw2017.apiary-mock.com/riwayat"

class RiwayatHelper:
	class __RiwayatHelper:
		def get_riwayat_list(self):
			response = requests.get(API_riwayat_list_URL)
			riwayat_list = response.json()
			return riwayat_list

	instance = None

	def __init__(self):
		if not RiwayatHelper.instance:
		   RiwayatHelper.instance = RiwayatHelper.__RiwayatHelper()
